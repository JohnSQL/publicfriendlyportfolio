
#%%[21]:
# Script reads a folder full of PDF format RBS Bankline statements and converts them to a standardised .csv

import pandas as pd
import camelot as cc
import numpy as np

import os
import shutil
import datetime as dt
from decimal import Decimal

def runIt (fileName, outDir, dateMagic = '0'):
        


    pdfData = cc.read_pdf(fileName
                        , table_areas=['0,820,600,30']
                        , columns=['70, 220, 270, 400, 490']
                        , pages = 'all'
                        , flavor = 'stream'
                        #, split_text=False
                        
                        )

    pdfHead = cc.read_pdf(fileName
                        , pages = '1'
                        , table_areas=['0,760,600,700']
                        , columns=['70, 270, 345']
                        ,  flavor = 'stream'
                        , split_text=True
                        )
    pdfSumm = cc.read_pdf(fileName, pages = '1', table_areas = ['0,800,600,700'], flavor = 'stream')
    pdSum = pdfSumm[0].df
    cBalDate = pdSum[0][0][-10:]

    #print("Statement loaded")

    #Use this to adjust the aim of the table read
    #thePlot = cc.plot(pdfHead[0], kind='contour')
    #thePlot.show


    i =0
    while i < len(pdfData):
        pdfData2 = pdfData[i].df
        print(i, pdfData2.shape)
        i= i+1



    i=0
    list1 = []
    df1=None
    for x in pdfData:
        df1 = x.df

        list1.append(df1) 
        i=i+1
    print(i, " tables valdated ")

    theHeader = pdfHead[0].df
    theSumm = pdfSumm[0].df


    fulldf=pd.concat(list1)

    # Return currency value
    strCurrency= theHeader.iat[theHeader.index[theHeader[2].str.contains('Currency')][0], 3]
    strCurrency=strCurrency.strip()

    # Return AccNo value
    strAccNo = theHeader.iat[theHeader.index[theHeader[0].str.contains('IBAN')][0], 1]
    strAccNo = strAccNo.strip()
    
    fulldf = fulldf.reset_index(drop=True)
    idxHeaderLine = fulldf.index[fulldf[0].str.contains('Date')][0]
    csvData2 = fulldf.loc[idxHeaderLine:, [0, 1, 2, 3, 4 , 5]]
    new_header = csvData2.iloc[0] #grab the first row for the header

    csvData2.columns = new_header #set the header row as the df header
    csvData2 = csvData2[1:] #take the data less the header row
    csvData2["bankAccountNumber"] = strAccNo
    csvData2["currencyLocal"] = strCurrency

    
    exportTab = csvData2.loc[csvData2['Date'].map(len) == 10]
    cols = ['Date', 'Narrative', 'Type', 'Debit', 'Credit', 'Ledger balance', 'bankAccountNumber', 'currencyLocal']
    exportTab2 = exportTab[cols]
    for x in cols:
        exportTab2[x] = exportTab2[x].str.replace(",","")
    exportTab2.loc[exportTab2['Debit'].str.len() >1, 'Debit'] = '-'+ exportTab2['Debit']
    exportTab2.loc[exportTab2['Debit'].str.len() >1, 'amount'] = exportTab2['Debit']
    exportTab2.loc[exportTab2['Credit'].str.len() >1, 'amount'] = exportTab2['Credit']
    exportTab2.loc[exportTab2['amount'].str.len() <1, 'amount'] = 0
    exportTab2['Ledger balance']=exportTab2['Ledger balance'].str.replace("Cr","")
    exportTab2.loc[exportTab2['Ledger balance'].str.contains("Dr"), 'Ledger balance'] = '-' + exportTab2['Ledger balance']
    exportTab2['Ledger balance']=exportTab2['Ledger balance'].str.replace("Dr","")
    exportTab2.loc[exportTab2['Ledger balance'].str.len() <1, 'Ledger balance'] = 0.00
    #exportTab2 = exportTab2.reset_index()
    exportTab2 = exportTab2.iloc[::-1].reset_index(drop = True)
    exportTab2.Date = pd.to_datetime(exportTab2.Date, format= '%d/%m/%Y')
    if dateMagic != '0':
        theDate = dt.datetime.strptime(dateMagic,'%Y-%m-%d')
        print(theDate)
        decSlice = exportTab2.loc[exportTab2.Date <= theDate]
        if decSlice.Date.max() + dt.timedelta(days = 3) < theDate:
            #%%sert a dummy row
            balIndex = decSlice.index.max()
            insertIndex = balIndex +1
            balVal = decSlice["Ledger balance"][balIndex]
            #print(balVal)
            newLine = pd.DataFrame({  "bankAccountNumber" : strAccNo
                                    , "currencyLocal" : strCurrency
                                    , "Date": theDate
                                    , "Narrative": f'Balance as at {theDate.strftime("%Y-%m-%d")}'
                                    , "amount": 0.00
                                    , "Ledger balance": balVal
                                    }
                                    , index = [insertIndex])
            exportTab2 = pd.concat([exportTab2.iloc[:insertIndex], newLine,exportTab2.iloc[insertIndex:]]).reset_index(drop = True)



    exportTab2['transactionDate'] = exportTab2.Date.dt.strftime('%Y-%m-%d')
    exportTab2['transactionType'] = exportTab2.Type
    exportTab2['transactionReference'] = exportTab2.Narrative
    exportTab2['balance'] = exportTab2['Ledger balance']
    exCols= ['bankAccountNumber'
            ,'transactionReference'
            ,'transactionType'
            ,'transactionDate'
            ,'currencyLocal'
            , 'amount'
            ,'balance'
            ]
    exportTab3 = exportTab2[exCols]

    exportFile = os.path.join(outDir, ('bs_'+strAccNo+'.csv'))
    exportCSV = exportTab3.to_csv(exportFile, index=False)
    shutil.copy(fileName,os.path.join(outDir, ('bs_'+strAccNo+'.pdf')))


inDir = r'c:\bank_statements\RBS'
dateMagic = '2019-12-31' #set to a date to add a row at this date
i=1
for theFile in os.listdir(inDir):
    if theFile.endswith('.pdf') and i>0:
        fileName = os.path.join(inDir, theFile)
        outDir = os.path.join(os.path.dirname(fileName), 'Out')
        os.makedirs(outDir, exist_ok=True)
        #exportFile = fileName + '.csv'
        runIt(fileName, outDir, dateMagic)
        
        i+=1
